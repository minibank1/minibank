# Desafío técnico Banco Ripley
## Descripción 📃

El proyecto fue desarrollado con la siguientes tecnologías para cada parte de
la arquitectura de la aplicación:

* Frontend:  Angular 11 + Typescript
* Microservicios:  NestJS + Trypescript + Express
* Base de datos: MySQL
* Despliegue microservicios: Google's Cloud Build y Cloud Run
* Despliegue frontend: Vercel

La paltaforma está disponible en [este link](https://fe-minibank.vercel.app/login), y crearte una cuenta. Para probar la funcionalidad de transeferencias bancarias puedes transferir a este RUT: 19.375.066-4

## Arquitectura 🐳

La plataforma está compuesta por una aplicación Frontend monolítica mas 3 Microservicios relacionados con la logica de negocio, autenticacion/autorización y lógica transaccional.


![picture](arch.png)

* MS-DATA: Es un microservicio que solo contiene lógica transaccional, se usa una libreria que expone endpoints CRUD para todas las entidades mapeadas con TypeORM los cuales son utilizados por otro microservicios.

* MS-PLATFORM: Es un microservicio que encapsula la lógica de negocios interno de MiniBank (transferencias, transacciones y consultas)

* MS-AUTHENTICATION: Microservicio que encapsula la lógica de autenticación y autorización, de la aplicación, tanto como para frontend como para otros microservicios. Por debajo utiliza Json web tokens.

## Despliege en entorno de desarrollo 🚀

Para todas las piezas es necesario tener instalada la última versión de Node con un gestor de paquetes que usted guste (yarn o npm).

* Microservicios:
1. Instalar dependencias (yarn, o npm install).
2. Copiar la información de .env-sample hacia .env (crear archivo).
3. Levantar las aplicación con `yarn start:dev` o `npm start:dev`

* Frontend:
1. Instalar dependencias (yarn, o npm install).
3. Levantar las aplicación con `yarn start` o `npm start`

## Modelo de datos 🗃

![picture](datamodel.png)

## To-Do ✅

- [ ] Test unitarios Microservicios / Frontend
- [ ] Autorización cruzada entre MS-PLATFORM y MS-AUTHENTICATION (auth guards)
- [ ] Envio de correos.
